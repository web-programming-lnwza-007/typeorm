import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepository = AppDataSource.getRepository(Product)
    
    const products = await productRepository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await productRepository.findOneBy({id:1})
    console.log(updateProduct)
    updateProduct.price=80;
    await productRepository.save(updateProduct)

}).catch(error => console.log(error))
