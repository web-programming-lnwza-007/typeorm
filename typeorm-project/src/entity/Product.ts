import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Product {
    @PrimaryGeneratedColumn({name:"Product_ID"})
    id:number;
    
    @Column({name:"Product_Name"})
    name:string;

    @Column({name:"Product_Price"})
    price:number;
}
